const fileRegex = /\.(vnz)$/

export default function htmx() {
  return {
    name: 'transform-file',

    transform(src: string, id: string) {
      console.log("vnz id = ", id)
      if (fileRegex.test(id)) {
      console.log("vnz src = ", src)
        return {
          code: src,
          map: null, // provide source map if available
        }
      }
    },
  }
}
