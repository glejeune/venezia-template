import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import htmx from "./htmx"

// https://vitejs.dev/config/
export default defineConfig({
  clearScreen: false,
  // assetsInclude: ['**/*.htmx'],
  plugins: [htmx()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  server: {
    host: "0.0.0.0",
    port: 1337,
    strictPort: true,
    proxy: {
      "^/v1": {
        target: "http://localhost:8000",
        ws: false,
      },
    },
  },
});

