import { Venezia } from "venezia"

export class XCounter extends Venezia {
  num: number = 0;
  timer?: number;

  constructor() {
    super();

    this.num = 42;
    this.watch('num', this.numChange)
  }

  render() {
    return `<button id="counter" type="button">count: ${this.num}</button>`
  }

  numChange(oldValue: number, newValue: number) {
    console.log("num", oldValue, newValue)
    this.reRender()
  }

  onClick() {
    this.num++

    if (this.timer) {
      clearTimeout(this.timer)
      this.timer = undefined
    }
    this.timer = setTimeout(() => {
      this.num = 42
    }, 6000)
  }

  stylesheet() {
    return `
    button {
      border-radius: 8px;
      border: 1px solid transparent;
      padding: 0.6em 1.2em;
      font-size: 1em;
      font-weight: 500;
      font-family: inherit;
      background-color: #1a1a1a;
      cursor: pointer;
      transition: border-color 0.25s;
    }
    button:hover {
      border-color: #646cff;
    }
    button:focus,
    button:focus-visible {
      outline: 4px auto -webkit-focus-ring-color;
    }
    `
  }
}

XCounter.component('x-counter')
