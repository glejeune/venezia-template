import { Venezia} from "venezia"

import typescriptLogo from '/typescript.svg'
import viteLogo from '/vite.svg'
import vnzLogo from '/venezia-32x32.svg'
import "./components/x-counter.ts"

export class App extends Venezia {
  render() { 
    return `
      <div>
        <a href="https://gitlab.com/glejeune/venezia/" target="_blank">
          <img src="${vnzLogo}" class="logo vanilla" alt="Venezia logo" />
        </a>
        <a href="https://vitejs.dev" target="_blank">
          <img src="${viteLogo}" class="logo" alt="Vite logo" />
        </a>
        <a href="https://www.typescriptlang.org/" target="_blank">
          <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript logo" />
        </a>
        <h1>Venezia + Vite + TypeScript</h1>
        <div class="card">
          <x-counter />
        </div>
        <p class="read-the-docs">
          Click on the Venezia, Vite and TypeScript logos to learn more
        </p>
      </div>
    `
  }
}

App.component('x-app')
