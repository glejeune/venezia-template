import { createApp } from 'venezia'
import './style.css'
import { App } from './app.ts'

const app = createApp(App)
app.mount("#app")
